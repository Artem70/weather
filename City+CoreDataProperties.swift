//
//  City+CoreDataProperties.swift
//  
//
//  Created by Artem Pashkevich on 05.09.17.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var name: String?

}
