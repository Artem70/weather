//
//  Wheather+CoreDataProperties.swift
//  
//
//  Created by Artem Pashkevich on 05.09.17.
//
//

import Foundation
import CoreData


extension Wheather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Wheather> {
        return NSFetchRequest<Wheather>(entityName: "Wheather")
    }

    @NSManaged public var humidity: Double
    @NSManaged public var icon: String
    @NSManaged public var pressure: Double
    @NSManaged public var temp_max: Double
    @NSManaged public var temp_min: Double
    @NSManaged public var temperature: Double
    @NSManaged public var weather_main: String
    @NSManaged public var city_name: String

}
