//
//  AppColor.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 31.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

class AppColor {
    
    static func blueColor() -> UIColor {
        return UIColor(displayP3Red: 45/255.0, green: 127/255.0, blue: 197/255.0, alpha: 1.0)
    }
    
    static func greyColor() -> UIColor {
        return UIColor(displayP3Red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0)
    }

}
