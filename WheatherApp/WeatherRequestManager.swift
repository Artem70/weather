//
//  WeatherRequestManager.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WheatherManager {
    
    // Singleton
    static let instance = WheatherManager()
    private init() {}
    
    static let baseUrl =  "http://api.openweathermap.org/data/2.5/weather?q="
    let key = "&appid=cc43de317c7b45042d6dd7d09ee12d74"
    
    var loadJSONproperties: ((Bool),(String)) -> Void = {_ in}
    
    func loadJSON (_ city: String) {
        
        let url = "\(WheatherManager.baseUrl)\(city)\(key)"
        
        print("1. start \(Thread.current)")
        
        Alamofire.request(url, method: .get).validate().responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print(json)
                
                print("3. writeDB \(Thread.current)")
                
                CoreDataStack.instance.saveJSONlist(city_name: json["name"].stringValue, humidity: json["main"]["humidity"].doubleValue, icon: json["weather"][0]["icon"].stringValue, pressure: json["main"]["pressure"].doubleValue, temp_max:  json["main"]["temp_max"].doubleValue, temp_min: json["main"]["temp_min"].doubleValue, temperature: json["main"]["temp"].doubleValue, weather_main:  json["weather"][0]["main"].stringValue)
                
                self.loadJSONproperties(true, "")
                
            case .failure(let error):
                print(error)
                self.loadJSONproperties(false, "\(error)")
            }
        }
        print("4. return \(Thread.current)")
    }
    
    func loadAllCityList() {
        
        var json:JSON!
        
        if let path : String = Bundle.main.path(forResource: "city.list", ofType: "json") {
            if let data = NSData(contentsOfFile: path) {
                json = JSON(data: data as Data)
            }
            
            for value in json {
                CoreDataStack.instance.saveCityName(name: value.1["name"].stringValue)
            }
            CoreDataStack.instance.saveContext()
        }
    }
}


