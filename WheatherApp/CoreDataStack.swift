//
//  CoreDataStack.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack {
    
    // Singleton
    static let instance = CoreDataStack()
    private init() {}
    
    
    func context () -> NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    func fetchAvailableCity() -> Bool {
        
        let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
        
        do {
            let items = try context().fetch(fetchRequest)
            
            if items.count > 0 {
                return false
            }
        } catch {
            print(error.localizedDescription)
        }
        return true
    }

    func fetchDataJSONlist() -> [Wheather] {
        
        var objects: [Wheather] = []
        
        let fetchRequest: NSFetchRequest<Wheather> = Wheather.fetchRequest()
        
        do {
            let items = try context().fetch(fetchRequest)
            objects = items
            
        } catch {
            print(error.localizedDescription)
        }
        return objects
    }

    
    
    func saveJSONlist (city_name: String, humidity: Double, icon: String, pressure: Double ,temp_max: Double, temp_min: Double, temperature: Double, weather_main: String) {
        
        //Создаем сущность
        guard let entity = NSEntityDescription.entity(forEntityName: "Wheather", in: context()) else{
            fatalError("can not create entity description")
        }
        
        //Создаем объект который хотим сохранить
        let object = NSManagedObject(entity: entity, insertInto: context()) as! Wheather
        
        object.city_name = city_name
        object.humidity = humidity
        object.icon = icon
        object.pressure = pressure
        object.temp_max = temp_max
        object.temp_min = temp_min
        object.temperature = temperature
        object.weather_main = weather_main
        
        //Сохраняем объект в контексте
        do {
            try object.managedObjectContext?.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
    func fetchDataAllCity() -> [City] {
        
        var objects: [City] = []
        
        let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
        
        do {
            let items = try context().fetch(fetchRequest)
            objects = items
            
        } catch {
            print(error.localizedDescription)
        }
        return objects
    }
    
    func fetchDataSearch (_ searchString: String?) -> [City] {
        
        var cityArray: [City] = []
        
        let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
        
        if let validString = searchString {
            fetchRequest.predicate = NSPredicate(format: "name CONTAINS[cd] %@", validString)
        }
        do {
            let items = try context().fetch(fetchRequest)
            cityArray = items
            
        } catch {
            print(error.localizedDescription)
        }
        return cityArray
    }


    func saveCityName(name: String) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "City", in: context()) else {
            fatalError("can not create entity description")
        }
        
        //Создаем объект который хотим сохранить
        let object = NSManagedObject(entity: entity, insertInto: context()) as! City
        
        object.name = name
    }

    
    func deleteAllCity() {
        
        let fetchRequest: NSFetchRequest<Wheather> = Wheather.fetchRequest()
        
        do {
            let result = try context().fetch(fetchRequest)
            
            for object in result {
                context().delete(object)
            }
            
            try context().save()
            print("saved!")
            
        } catch {
            print(error.localizedDescription)
        }
    }

    

// MARK: - Core Data stack

lazy var persistentContainer: NSPersistentContainer = {
    /*
     The persistent container for the application. This implementation
     creates and returns a container, having loaded the store for the
     application to it. This property is optional since there are legitimate
     error conditions that could cause the creation of the store to fail.
     */
    let container = NSPersistentContainer(name: "WheatherApp")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        if let error = error as NSError? {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            /*
             Typical reasons for an error here include:
             * The parent directory does not exist, cannot be created, or disallows writing.
             * The persistent store is not accessible, due to permissions or data protection when the device is locked.
             * The device is out of space.
             * The store could not be migrated to the current model version.
             Check the error message to determine what the actual problem was.
             */
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    })
    return container
}()

// MARK: - Core Data Saving support

func saveContext () {
    let context = persistentContainer.viewContext
    if context.hasChanges {
        do {
            try context.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
}

}
