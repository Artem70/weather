//
//  ListViewController.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import PKHUD

class ListViewController: BaseController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    
    fileprivate var filteredArray: [Wheather] = []
    
    func setupLoadMenu()  {
        self.tableView.reloadData()
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.show(.progress)
        setupView()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadCityList()
        HUD.hide()
    }
}


//MARK: UITableViewDataSource, UITableViewDelegate
extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let wheather = filteredArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ListViewCell.reuseIdentifier(), for: indexPath) as! ListViewCell
        
        cell.wheather = wheather
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true )
        let vc = ShowViewController()
        vc.cityAttribute = filteredArray[indexPath.row]
        present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let context = CoreDataStack.instance.persistentContainer.viewContext
        
        if editingStyle == .delete {
            
            context.delete(filteredArray[indexPath.row])
            
            CoreDataStack.instance.saveContext()
            
            do {
                filteredArray = try context.fetch(Wheather.fetchRequest())
            } catch {
                print(error.localizedDescription)
            }
            tableView.reloadData()
        }
    }
}

//MARK: LoadData
private extension ListViewController {
    
    func loadCityList() {
        
        if CoreDataStack.instance.fetchAvailableCity() {
            WheatherManager.instance.loadAllCityList()
        }
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func loadData() {
        filteredArray = CoreDataStack.instance.fetchDataJSONlist()
    }
}

//MARK: Setup
private extension ListViewController {
    
    func setupView() {
        
        setupBarButtonItems()
        setupTableView()
    }
    
    func setupBarButtonItems() {
        
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: #selector(addButtonPressed(_:)), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
        self.navigationController?.isNavigationBarHidden = true
        self.navigationItem.title = "Избранное"

    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView()
        tableView.register(ListViewCell.nib(),
                           forCellReuseIdentifier: ListViewCell.reuseIdentifier())
    }
}

//MARK: Actions
private extension ListViewController {
    
    @objc func addButtonPressed(_ sender: UIButton) {
        
        let vc = AddCityController()
        vc.cityDelegate = self
        vc.filteredArray = filteredArray
        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: BookListDelegate
extension ListViewController: AddCityControllerDelegate {
    
    func addCityValue(value: String) {
        self.loadData()
        tableView.reloadData()
    }
}




