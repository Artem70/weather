//
//  ListViewCell.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class ListViewCell: UITableViewCell {
    
    var city: City? {
        didSet {
            applyData()
        }
    }

    var wheather: Wheather? {
        didSet {
            apply()
        }
    }

    
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    private func applyData() {
        guard let validcity = city else {
            return
        }
        label.text = validcity.name
    }
    
    private func apply() {
        guard let validWheather = wheather else {
            return
        }
        label.text = validWheather.city_name
        let url = "http://openweathermap.org/img/w/\(validWheather.icon).png"
        ImageView.imageFromUrl(urlString: url)
    }

}
