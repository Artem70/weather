//
//  BaseController.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class BaseController: UIViewController {
    
    enum BackgroundStyle {
        case green
        case white
        
        func color() -> UIColor {
            switch self {
            case .green:
                return UIColor.green
            case .white:
                return UIColor.white
            }
        }
    }
    
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func preferredBackgroundStyle() -> BackgroundStyle {
        return .green
    }
}

