//
//  AppDelegate.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import PKHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(3*60*60)
        
        setupApp()
        load()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        CoreDataStack.instance.saveContext()
    }
    
    //MARK: Support for background fetch
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let cityList = CoreDataStack.instance.fetchDataJSONlist()
        
        let names = cityList.map( {$0.city_name} )
    
        CoreDataStack.instance.deleteAllCity()
        
        names.forEach({
            WheatherManager.instance.loadJSON($0)
        })
        
        completionHandler(.newData)
    }
}

//MARK: Setup
private extension AppDelegate {
    
    func setupApp() {
        
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = AppColor.blueColor()
        
        if let barFont = UIFont(name: "AppleSDGothicNeo-Light", size: 24) {
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:AppColor.blueColor(), NSFontAttributeName: barFont]
        }
    }
    
    func load() {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = NavBarController.createNavBar(viewController: ListViewController())
        window?.makeKeyAndVisible()
    }
}
    
