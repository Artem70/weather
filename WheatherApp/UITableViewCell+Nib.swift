//
//  UITableViewCell+Nib.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    static func nib() -> UINib {
        let name = NSStringFromClass(self)
        
        let arrayName = name.components(separatedBy: ".")
        
        guard let clearName = arrayName.last else {
            fatalError()
        }
        
        return UINib(nibName: clearName, bundle: Bundle.main)
    }
}
