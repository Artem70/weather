//
//  AddCityController.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import PKHUD

protocol AddCityControllerDelegate: class {
    
    func addCityValue(value: String)
}

class AddCityController: BaseController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    weak var cityDelegate: AddCityControllerDelegate?
    
    fileprivate var cityArray: [City] = []
    var filteredArray: [Wheather]?
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        reloadDataCity()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
}

//MARK: reloadDataCity
private extension AddCityController {
    
    func reloadDataCity() {
        
            if searchBar.text == ""{
                cityArray = []
            }else{
                cityArray = CoreDataStack.instance.fetchDataSearch(searchBar.text!)
            }
        tableView.reloadData()
    }
}

//MARK: Setup
private extension AddCityController {
    
    func setupView() {
        self.navigationItem.title = "Список Городов"
        self.reloadDataCity()
        setupTableView()
        searchBarSetup()
    }
    
    func searchBarSetup() {
        searchBar.delegate = self
        searchBar.barTintColor = AppColor.greyColor()
        searchBar.tintColor = AppColor.blueColor()
    }
    
    func setupTableView() {
        tableView.register(ListViewCell.nib(),forCellReuseIdentifier: ListViewCell.reuseIdentifier())
        tableView.tableHeaderView = searchBar
        tableView.tableFooterView = UIView()
    }
}


//MARK: UITableViewDataSource
extension AddCityController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let city = cityArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ListViewCell.reuseIdentifier(), for: indexPath) as! ListViewCell
        
        cell.city = city
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true )
        
        guard let cityName = cityArray[indexPath.row].name else {
            fatalError()
        }
        
        guard let filter = filteredArray else {
            return
        }
        
        for value in filter {
            if value.city_name == cityName {
                showAlertError(mesageTitle: "Error", message: "Город уже добавлен")
                 return
            }
        }
        
        searchBar.text = nil
        
        HUD.show(.progress)
        
        WheatherManager.instance.loadJSON(cityName)
        
        WheatherManager.instance.loadJSONproperties = ({ [weak self] result, string in
            
            guard let strongSelf = self else {
                return
            }
            
            if result == true {
                
                strongSelf.cityDelegate?.addCityValue(value: cityName)
                HUD.hide()
                strongSelf.navigationController?.popViewController(animated: true)
                
            }else{
                strongSelf.showAlertError(mesageTitle: "Error", message: string)
                HUD.hide()
            }
        })
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityArray.count
    }
}

//MARK: Search
extension AddCityController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadDataCity()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        view.endEditing(true)
        searchBar.text = nil
        reloadDataCity()
    }
}
