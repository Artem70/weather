//
//  UIViewController+UIAlertController.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 25.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlertError(mesageTitle: String, message: String) {
        let alertERROR = UIAlertController(title: mesageTitle , message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok" , style: .cancel, handler: nil)
        alertERROR.addAction(okAction)
        alertERROR.setValue(NSAttributedString(string: mesageTitle, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 20),NSForegroundColorAttributeName : UIColor.red]), forKey: "attributedTitle")
        self.present(alertERROR, animated: true, completion: nil)
    }
}
