//
//  WheatherSettings.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 21.09.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

extension Wheather {
    
    var pressureString: String {
        return "Pressure: \(Int(pressure * 0.750062)) mm"
    }
    
    var humidityString: String {
        return "Humidity: \(Int(humidity)) %"
    }
    
    var temperatureString: String {
        return "\(Int(temperature - 273.15))˚C"
    }
    
    var tempMinString: String {
        return "Temperature \n Minimum: \(Int(temp_min - 273.15))˚C"
    }
    
    var tempMaxString: String {
        return "Temperature \n Maximum: \(Int(temp_max - 273.15))˚C"
    }
}
