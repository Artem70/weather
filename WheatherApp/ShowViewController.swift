//
//  ShowViewController.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class ShowViewController: BaseController {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var wheatherMainLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var tempMinLabel: UILabel!
    @IBOutlet weak var tempMaxLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var backImage: UIImageView!
    
    var cityAttribute: Wheather!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        loadData()
    }
}


//MARK: Setup
extension ShowViewController {
    
    func setup() {
        changeBackImage()
    }
    
    func changeBackImage() {
        switch cityAttribute.icon {
        case "09d", "09n", "10d", "10n", "11d", "11n":
            backImage.image = UIImage(named: "rain.png")
            
        case "13d", "13n":
            backImage.image = UIImage(named: "winter.png")
            
            //        case "02d", "02n", "03d", "03n", "04d", "04n":
            //            backImage.image = UIImage(named: "greyFall.png")
            //        case "01d", "01":
        //            backImage.image = UIImage(named: "sun.png")
        default:
            backImage.image = UIImage(named: "sun.png")
        }
    }
}

//MARK: LoadData
extension ShowViewController {
    
    func loadData() {
        guard let city = cityAttribute else {
            fatalError()
        }
        
        cityNameLabel.text = city.city_name
        temperatureLabel.text = city.temperatureString
        tempMinLabel.text = city.tempMaxString
        tempMaxLabel.text = city.tempMinString
        humidityLabel.text = city.humidityString
        pressureLabel.text = city.pressureString
        wheatherMainLabel.text = city.weather_main
        
        iconImage.image = UIImage(named: "\(city.icon).png")
    }
}

//MARK: Action
extension ShowViewController {
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}




