//
//  NavBarController.swift
//  WheatherApp
//
//  Created by Artem Pashkevich on 29.08.17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import UIKit

class NavBarController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    static func createNavBar(viewController: UIViewController) -> NavBarController {
        return NavBarController(rootViewController: viewController)
    }
}
